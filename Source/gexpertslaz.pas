{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit GExpertsLaz;

interface

uses
  GX_Backup, GX_BackupConfig, GX_BackupOptions, GX_Zipper, 
  GX_ClipboardHistory, GX_ClipboardOptions, GX_CodeLib, GX_CodeOpt, 
  GX_CodeSrch, GX_ProofreaderAutoCorrectEntry, GX_ProofreaderConfig, 
  GX_ProofreaderCorrection, GX_ProofreaderData, GX_ProofreaderDefaults, 
  GX_ProofreaderExpert, GX_ProofreaderKeyboard, GX_ProofreaderUtils, 
  GX_eAlign, GX_eAlignOptions, GX_eChangeCase, GX_eComment, GX_eDate, 
  GX_EditorExpert, GX_EditorExpertManager, GX_eFindDelimiter, 
  GX_ePrevNextIdentifier, GX_eQuote, GX_eQuoteSupport, GX_eReverseStatement, 
  GX_eSelectIdentifier, GX_eSelectionEditorExpert, GX_eUsesManager, 
  GX_MenusForEditorExpert, GX_Toolbar, GX_ToolbarConfig, GX_ToolBarDropDown, 
  GX_AsciiChart, GX_CleanDirectories, GX_ComponentGrid, GX_CompsToCode, 
  GX_CopyComponentNames, GX_ExpertManager, GX_FindComponentRef, 
  GX_HideNonVisualComps, GX_IdeShortCuts, GX_PeInfo, GX_PeInformation, 
  GX_PerfectLayout, GX_SelectComponents, GX_SetFocusControl, GX_TabOrder, 
  GX_FavFileProp, GX_FavFiles, GX_FavFolderProp, GX_FavNewFolder, 
  GX_FavOptions, GX_FavUtil, GX_About, GX_ActionBroker, GX_Actions, 
  GX_BaseForm, GX_ClassHacks, GX_ClassMgr, GX_ConfigurationInfo, GX_Configure, 
  GX_Consts, GX_DbugIntf, GX_Debug, GX_DesignerMenu, GX_EditorChangeServices, 
  GX_EditorEnhancements, GX_EditorFormServices, GX_EditorShortcut, 
  GX_EditReader, GX_Experts, GX_FeedbackWizard, GX_FileScanner, 
  GX_GetIdeVersion, GX_GExperts, GX_IconMessageBox, GX_KbdShortCutBroker, 
  GX_KibitzComp, GX_LibrarySource, GX_MacroParser, GX_MenuActions, 
  GX_MessageBox, GX_PackageSource, GX_Progress, GX_SharedImages, 
  GX_SynMemoUtils, GX_UnitPositions, GX_UsesManager, GX_VerDepConst, 
  GX_IdeEnhance, GX_IdeFormEnhancer, GX_MultilineHost, GX_MultiLinePalette, 
  GX_IdeBaseDock, GX_IdeDeskForm, GX_IdeDeskUtil, GX_IdeDock, GX_IdeDockForm, 
  GX_IdeDockDemo, GX_MacroLibrary, GX_MacroLibraryConfig, 
  GX_MacroLibraryNamePrompt, GX_MacroExpandNotifier, GX_MacroFile, 
  GX_MacroSelect, GX_MacroTemplateEdit, GX_MacroTemplates, 
  GX_MacroTemplatesExpert, GX_MessageDialog, GX_MessageOptions, GX_OpenFile, 
  GX_OpenFileConfig, GX_ProcedureList, GX_ProcedureListOptions, GX_ProjDepend, 
  GX_ProjDependFilter, GX_ProjDependProp, GX_ProjOptionSets, GX_ProjOptMap, 
  GX_CompRename, GX_CompRenameAdvanced, GX_CompRenameConfig, GX_ReplaceComp, 
  GX_ReplaceCompData, GX_ReplaceCompLog, GX_ReplaceCompMapDets, 
  GX_ReplaceCompMapGrpList, GX_ReplaceCompMapList, GX_ReplaceCompUtils, 
  GX_eSampleEditorExpert, GX_SampleExpert, GX_SetComponentProps, 
  GX_SetComponentPropsConfig, GX_SetComponentPropsStatus, GX_SourceExport, 
  GX_SourceExportOptions, DebugOptions, DebugWindow, TrayIcon, GX_ToDo, 
  GX_ToDoOptions, GX_GenericClasses, GX_GxUtils, GX_IdeUtils, GX_XmlUtils, 
  GX_ClassBrowser, GX_ClassIdentify, GX_ClassOptions, GX_ClassParsing, 
  GX_ClassProp, GX_ClassReport, GX_GrepBackend, GX_GrepExpert, GX_GrepOptions, 
  GX_GrepPrinting, GX_GrepRegExSearch, GX_GrepReplace, GX_GrepResults, 
  GX_GrepResultsOptions, GX_GrepSearch, GX_Replace, GX_GenericUtils, 
  GX_OtaUtils, GX_RichEditUtils, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('TrayIcon', @TrayIcon.Register);
end;

initialization
  RegisterPackage('GExpertsLaz', @Register);
end.
