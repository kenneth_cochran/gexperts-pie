unit GX_RichEditUtils;

interface

uses
  ComCtrls;

procedure CenterLineInEdit(RichEdit: TRichEdit; LineNum: Integer);



implementation

uses
  Types,
  Classes,
  SysUtils,
  Math,
  Windows,
  Messages;

const
  RichEdit10ModuleName = 'RICHED32.DLL';
  RichEdit20ModuleName = 'RICHED20.DLL';

var
  RichEditVersion: Integer;

// Based on function TJvCustomRichEdit.GetCharPos(CharIndex: Integer): TPoint;
function GetCharPos(RichEdit: TRichEdit; CharIndex: Integer): TPoint;
var
  Res: Longint;
begin
  FillChar(Result, SizeOf(Result), 0);
  if RichEdit.HandleAllocated then
  begin
    if RichEditVersion = 2 then
    begin
      Res := SendMessage(RichEdit.Handle, Messages.EM_POSFROMCHAR, CharIndex, 0);
      Result.X := LoWord(Res);
      Result.Y := HiWord(Res);
    end
    else { RichEdit 1.0 and 3.0 }
      SendMessage(RichEdit.Handle, Messages.EM_POSFROMCHAR, WPARAM(@Result), CharIndex);
  end;
end;


procedure CenterLineInEdit(RichEdit: TRichEdit; LineNum: Integer);
// I don't know the reason, but the RichEdit 2 control in VCL does not
// respond to the EM_SCROLLCARET in Richedit.h but it does so to the
// constant in WinUser.h
const
  EM_SCROLLCARET  = $00B7;
var
  TextPos: lResult;
  Pos: TPoint;
begin
  TextPos := SendMessage(RichEdit.Handle, EM_LINEINDEX, LineNum, 0);

  if TextPos <> -1 then begin
    // Go to top
    SendMessage(RichEdit.Handle, EM_SETSEL, 0, 0);
    SendMessage(RichEdit.Handle, EM_SCROLLCARET, 0, 0);

    // Get the coordinates for the beginning of the line
    Pos := GetCharPos(RichEdit, TextPos);

    // Scroll from the top
    SendMessage(RichEdit.Handle, WM_VSCROLL,
        MakeWParam(SB_THUMBPOSITION, Max(0, Pos.y - (RichEdit.ClientHeight div 2))), 0);

    // Optionally set the caret to the beginning of the line
    SendMessage(RichEdit.Handle, EM_SETSEL, TextPos, TextPos);
  end;
end;

// Initialization variables
var
  OldError: Longint;
  FLibHandle: THandle;
  Ver: TOsVersionInfo;

initialization

  // Get the RichEditVersion.  Code modified from JvRichEd.
  RichEditVersion := 1;
  OldError := SetErrorMode(SEM_NOOPENFILEERRORBOX);
  try
    FLibHandle := LoadLibrary(RichEdit20ModuleName);
    if (FLibHandle > 0) and (FLibHandle < HINSTANCE_ERROR) then FLibHandle := 0;
    if FLibHandle = 0 then begin
      FLibHandle := LoadLibrary(RichEdit10ModuleName);
      if (FLibHandle > 0) and (FLibHandle < HINSTANCE_ERROR) then FLibHandle := 0;
    end
    else begin
      RichEditVersion := 2;
      Ver.dwOSVersionInfoSize := SizeOf(Ver);
      GetVersionEx(Ver);
      with Ver do begin
        if (dwPlatformId = VER_PLATFORM_WIN32_NT) and
          (dwMajorVersion >= 5) then
          RichEditVersion := 3;
      end;
    end;
  finally
    SetErrorMode(OldError);
  end;

finalization

  if FLibHandle <> 0 then FreeLibrary(FLibHandle);

end.

